var argv = require('yargs').argv;
path = argv.path;


var
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    inputSass = '../../../wp-content/themes/bones2017/library/scss/style.scss',
    inputCss = '../../../wp-content/themes/bones2017/library/css/stylefixed.css',
    outputCss = '../../../wp-content/themes/bones2017/library/css/',
    inputCssLaywer = '/Applications/XAMPP/xamppfiles/htdocs/lawyer/css/minify/',
    outputCssLaywer = '/Applications/XAMPP/xamppfiles/htdocs/lawyer/css/minify/',
    sassGlob = require ('gulp-sass-glob'),
    prefix = require('gulp-autoprefixer'),
    jsmin = require('gulp-jsmin'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css'),
        cssConCat = require ('gulp-concat-css'),
    lint = require('gulp-sass-lint'),
    sourcemaps = require('gulp-sourcemaps'),
    rename = require('gulp-rename');

//clean css
gulp.task('minify-css', function() {
    return gulp
        .src('/Applications/XAMPP/xamppfiles/htdocs/lawyer/css/minify/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(prefix)
        .pipe(cssConCat(inputCssLaywer))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(outputCssLaywer));
});

//sass
gulp.task('sass', function () {
    gulp.src(inputSass)
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(sass({ style: 'expanded' }))
        .pipe(prefix("last 3 version", "safari 5", "ie 8", "ie 9"))
        .pipe(sass().on('error', sass.logError))
        .pipe(rename({suffix: '.min'}))
        .pipe(cleanCSS({compatiblity:'ie8'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(outputCss));
});
gulp.task('watch', function () {
    gulp.watch ('../../../wp-content/themes/bones2017/library/**/*.scss', ['sass'])
});

//minify js
gulp.task('jsmin', function () {
    gulp.src('../*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('../dist/'));
});

//default task
// Add import from config file var tasklist[]
gulp.task('default', ['sass']);